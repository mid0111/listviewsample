package com.mid.listviewsample.app;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

/**
 * Created by mid on 14/04/26.
 */
public class ListViewSample extends View {
    public ListViewSample(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        TextView textView = new TextView(this.getContext());
        textView.setTextSize(20);
        textView.setText("ほげほげ");
    }
}
